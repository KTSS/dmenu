# Luke's dmenu

Extra stuff added to vanilla dmenu:

- reads Xresources (ergo pywal compatible)
- alpha patch, which importantly allows this build to be embedded in transparent st
- can view color characters like emoji (libxft-bgra is required for this reason)
- `-P` for password mode: hide user input
- `-r` to reject non-matching input
- dmenu options are mouse clickable

# Sennin's dmenu

Even more stuff added

- (4.9) Fuzzy highlighting (This patch make it so that fuzzy matches gets highlighted and is therefore meant to be used together with the patch fuzzymatch.)
- (4.9) Fuzzy match (This patch adds support for fuzzy-matching to dmenu, allowing users to type non-consecutive portions of the string to be matched.)
- (4.7) Instant (when there is only one result left, dmenu automatically picks it, no need to type enter)
- (4.9) Numbers (adds ammount of results possible and total)
- (5.0) Solarized dark theme (I like it =) )

## Installation

You must have `libxft-bgra` installed until the libxft upstream is fixed.

After making any config changes that you want, but `make`, `sudo make install` it.
